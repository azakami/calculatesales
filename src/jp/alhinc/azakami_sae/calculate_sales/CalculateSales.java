package jp.alhinc.azakami_sae.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map.Entry;

public class CalculateSales {

	public static void main(String[] args) {
		//使用するマップはこれ
		HashMap<String, String> branchData = new HashMap<String, String>();
		HashMap<String, Long> salesData = new HashMap<String, Long>();

		//System.out.println("ここにあるファイルを開きます =>" + args[0]);

		File file = new File(args[0], "branch.lst");
		if (!file.exists()) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}

		//①支店定義ファイル読み込み
		BufferedReader br = null;
		try {
			br = new BufferedReader(new FileReader(file));

			//一行ずつ読む
			String branch;

			while ((branch = br.readLine()) != null) {

				//フォーマットのエラー処理
				if (!(branch.matches("[0-9]{3},.*$"))) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				//読み込んだ行を","で2つに分ける
				String[] branchList = branch.split(",");
				String storeCode = branchList[0];
				String storeName = branchList[1];

				//マップに値を入れる
				branchData.put(storeCode, storeName);
				salesData.put(storeCode, 0L);
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		} finally {
			if (br != null) {
				try {
					br.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}

		//②売上ファイルを集計
		//System.out.println("ここにあるファイルを集計します =>" + args[0]);

		//フィルターを作ります
		FilenameFilter filter = new FilenameFilter() {

			// フィルターの中身はこれ
			public boolean accept(File directory, String filename) {
				File file = new File(directory,filename );
				return filename.matches("[0-9]{8}.rcd") && file.isFile();

			}
		};

		//フィルターを使用して一覧を取得する
		File[] salesFiles = new File(args[0]).listFiles(filter);

		Arrays.sort(salesFiles);

		//連番確認しましょう
		String minIndex = salesFiles[0].getName().substring(0, 8);
		String maxIndex = salesFiles[salesFiles.length - 1].getName().substring(0, 8);

		int min = Integer.parseInt(minIndex);
		int max = Integer.parseInt(maxIndex);

		if (min + (salesFiles.length - 1) != max) {
			System.out.println("売上ファイル名が連番になっていません");
			return;
		}

		//フィルターで取得したファイルを読み込む
		for (int i = 0; i < salesFiles.length; i++) {
			String fileName = salesFiles[i].getName();

			try {
				br = new BufferedReader(new FileReader(salesFiles[i]));

				String storeCode = br.readLine();
				String storeSale = br.readLine();

				//支店コードに不備はないかな
				if (branchData.get(storeCode) == null) {
					System.out.println(fileName + "の支店コードが不正です");
					return;
				}

				//3行目があった場合のエラー処理
				if (br.readLine() != null) {
					System.out.println(fileName + "のフォーマットが不正です");
					return;
				}

				//足し算していこう
				long saleNumber = Long.parseLong(storeSale);
				Long storeSum = salesData.get(storeCode) + saleNumber;

				//合計金額についてのエラー処理
				if (storeSum.toString().length() > 10) {
					System.out.println("合計金額が10桁を超えました");
					return;
				}

				//mapに値を保存
				salesData.put(storeCode, storeSum);

			} catch (IOException e) {
				System.out.println("予期せぬエラーが発生しました");
				return;

			} finally {
				if (br != null) {
					try {
						br.close();
					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}
				}
			}
		}

		//System.out.println("ここにあるファイル \"branch.out\"に集計結果が出力されます =>" + args[0]);
		//③集計結果出力します
		BufferedWriter bw = null;

		try {
			bw = new BufferedWriter(new FileWriter(new File(args[0], "branch.out")));

			for (Entry<String, String> aggregate : branchData.entrySet()) {
				String code = aggregate.getKey();
				String name = aggregate.getValue();
				Long sale = salesData.get(code);

				bw.write(code + "," + name + "," + sale);
				bw.newLine();
			}
		} catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;
		} finally {
			if (bw != null) {
				try {
					bw.close();
				} catch (IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}
